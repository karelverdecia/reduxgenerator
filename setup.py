from setuptools import setup, find_packages
import sys, os

version = '0.1'

setup(name='reduxgenerator',
      version=version,
      description="Python tool for generating javascript redux actions, reducers and selectors.",
      long_description="""\
""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='JavaScript Redux',
      author='Karel Antonio Verdecia Ortiz',
      author_email='kverdecia@gmail.com',
      url='',
      license='LGPL3',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
