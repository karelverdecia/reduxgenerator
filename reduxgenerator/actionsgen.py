# -*- coding: utf8 -*-
from io import StringIO
from . import parameters
from .mixins import GenMixin


class ActionGen(GenMixin):
    def __init__(self, action):
        assert isinstance(action, parameters.Action)
        self._action = action

    @property
    def name(self):
        items = self._action.type.lower().split('_')
        items = [i.capitalize() for i in items]
        items[0] = items[0].lower()
        return ''.join(items)

    def write_simple_body(self):
        self.write('    return {\n')
        self.write('        type: {}'.format(self._action.type))
        if self._action.parameters:
            self.write(',')
        self.write('\n')
        if self._action.parameters:
            self.write('        {}\n'.format(self._action.parameters))
        self.write('    }\n')

    def write_data_selector(self):
        data_selector = self._action.data_selector
        if not data_selector:
            return False
        data_selector = data_selector.strip()
        if not data_selector.endswith(';') and not data_selector.endswith(')'):
            data_selector += '(state)'
        self.writeln('let data = Reducers.{};'.format(data_selector))
        return True

    def write_loading_selector(self):
        loading_selector = self._action.loading_selector
        if not loading_selector:
            return False
        loading_selector = loading_selector.strip()
        if not loading_selector.endswith(';') and not loading_selector.endswith(')'):
            loading_selector += '(state)'
        self.writeln('let isLoading = Reducers.{};'.format(loading_selector))
        return True

    def write_cancel(self, has_data, has_loading):
        vars = []
        if has_data:
            vars.append('data')
        if has_loading:
            vars.append('isLoading')
        if vars:
            self.write('if ({})'.format(' || '.join(vars)))
            self.writeln('{')
            with self.level_up():
                self.writeln('console.log("{} canceled.");'.format(self._action.type))
                self.writeln('return;')
            self.writeln('}')

    def write_start_dispatch(self):
        action = self._action.start_action
        if action:
            self.writeln('dispatch({}({}));'.format(action.action_name, action.parameters or ''))

    def write_error_dispatch(self):
        action = self._action.error_action
        if action:
            self.writeln('        dispatch({}({}));'.format(action.action_name,
                action.parameters or 'message'))

    def write_success_dispatch(self):
        action = self._action.success_action
        if action:
            self.writeln('        dispatch({}(res.body));'.format(action.action_name))

    def write_ajax_request(self):
        self.writeln('Request')
        with self.level_up():
            self.writeln('.{}(url)'.format(self._action.method))
            if self._action.use_csrf:
                self.writeln(".set('X-CSRFToken', csrftoken)")
                self.writeln(".send({})".format(self._action.parameters))
            self.writeln(".end((err, res) => {")
            self.writeln("    if (err || !res.body) {")
            self.writeln("        let message = '{} error:';".format(self._action.type))
            self.writeln("        console.log(message, err);")
            self.write_error_dispatch()
            self.writeln("    }")
            self.writeln("    else {")
            self.write_success_dispatch()
            self.writeln("    }")
            self.writeln("});")


    def write_ajax_body(self):
        with self.level_up():
            self.writeln('return function(dispatch, getState) {')
            with self.level_up():
                self.writeln('let state = getState();')
                self.write_cancel(self.write_data_selector(), self.write_loading_selector())
                self.writeln('let url = `{}`;'.format(self._action.url))
                if self._action.use_csrf:
                    self.writeln("let csrftoken = Cookies.get('csrftoken');")
                self.write_start_dispatch()
                self.write_ajax_request()
            self.writeln('}')

    def write_body(self):
        action = self._action
        self.write('export function {}({})'.format(self.name, action.parameters or ''))
        self.write('{\n')
        if self._action.url:
            self.write_ajax_body()
        else:
            self.write_simple_body()
        self.write('}\n')


class ModuleActionsGen(GenMixin):
    def __init__(self, module, actions):
        assert isinstance(actions, parameters.Actions)
        self._actions = actions.module_actions(module)

    def write_header(self):
        self.writeln("import Request from 'superagent';")
        if self._actions.use_csrf:
            self.writeln("import Cookies from 'js-cookie';")
        self.writeln("import * as Reducers from '../reducers';")
        self.writeln("import {")
        with self.level_up():
            actions = self._actions.without_url
            for group in actions.groups:
                self.writeln(', '.join(actions.group_actions(group))+',')
        self.writeln("} from '../action-types';")
        self.writeln()

    def write_body(self):
        self.write_header()
        for action in self._actions.values():
            action_gen = ActionGen(action)
            self.write(action_gen())
            self.write('\n')


class IndexGen(GenMixin):
    def __init__(self, actions):
        assert isinstance(actions, parameters.Actions)
        self._actions = actions

    def write_body(self):
        for module in self._actions.modules:
            self.writeln('export {')
            with self.level_up():
                for action in self._actions.module_actions(module).values():
                    self.writeln(action.action_name+',')
            self.write('} ')
            self.writeln("from './{}';".format(module))


class ActionsGen(GenMixin):
    def __init__(self, params):
        assert isinstance(params, parameters.Parameters)
        self._actions = params.actions

    def __call__(self):
        index_gen = IndexGen(self._actions)
        yield ('actions/index.js', index_gen())
        for module in self._actions.modules:
            actions_gen = ModuleActionsGen(module, self._actions)
            yield ('actions/{}.js'.format(module), actions_gen())

