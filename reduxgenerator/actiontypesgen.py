# -*- coding: utf8 -*-
from io import StringIO
from .mixins import GenMixin
from . import parameters


class ActionTypes(GenMixin):
    def __init__(self, actions):
        assert isinstance(actions, parameters.Actions)
        self._actions = actions

    def __call__(self):
        self._buffer = StringIO()
        actions = self._actions.without_url
        for module in actions.modules:
            self.write("// module: {}\n".format(module))
            module_actions = actions.module_actions(module)
            for group in module_actions.groups:
                group_actions = module_actions.group_actions(group)
                for action in group_actions.values():
                    self.write("export const {0} = '{0}';\n".format(action.type))
                self.write('\n')
        return self._buffer.getvalue()

