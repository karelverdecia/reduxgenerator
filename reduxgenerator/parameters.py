# -*- coding: utf8 -*-
from collections import OrderedDict, Mapping
from nltk.stem import WordNetLemmatizer


wordnet_lemmatizer = WordNetLemmatizer()


class Actions(Mapping):
    def __init__(self, data, params):
        data = [] if data is None else data['actions']
        self._actions = OrderedDict()
        for item in data:
            action = Action(item, self)
            if action in self:
                raise ValueError("repeated action types: {}".format(action.type))
            self._actions[action.type] = action
        assert isinstance(params, Parameters)
        self._params = params

    def __getitem__(self, type_name):
        return self._actions[type_name]

    def __len__(self):
        return len(self._actions)

    def __iter__(self):
        return iter(self._actions)

    def __contains__(self, value):
        if isinstance(value, basestring):
            return value in self._actions
        return value.type in self._actions

    def add(self, action):
        if action in self:
            raise ValueError("Action with type {} already exist.".format(action.type))
        self._actions[action.type] = action

    def filter(self, condition):
        result = Actions(None, self._params)
        for action in self.values():
            if condition(action):
                result.add(action)
        return result

    @property
    def with_url(self):
        return self.filter(lambda action: action.url is not None)

    @property
    def without_url(self):
        return self.filter(lambda action: action.url is None)

    @property
    def modules(self):
        result = []
        for action in self.values():
            if action.module not in result:
                result.append(action.module)
        return result

    def module_actions(self, module):
        return self.filter(lambda action: action.module == module)

    @property
    def groups(self):
        result = []
        for action in self.values():
            if action.group not in result:
                result.append(action.group)
        return result

    def group_actions(self, group):
        return self.filter(lambda action: action.group == group)

    @property
    def states(self):
        result = []
        for action in self.values():
            if action.state not in result:
                result.append(action.state)
        return result

    def state_actions(self, state):
        return self.filter(lambda action: action.state == state)

    @property
    def use_csrf(self):
        for action in self.values():
            if action.use_csrf:
                return True
        return False


class Action(object):
    def __init__(self, data, actions):
        self._data = data
        self._actions = actions

    @property
    def type(self):
        return self._data['type']

    @property
    def action_name(self):
        items = self.type.lower().split('_')
        items = [i.capitalize() for i in items]
        items[0] = items[0].lower()
        return ''.join(items)

    @property
    def module(self):
        return self._data['module']

    @property
    def parameters(self):
        return self._data.get('parameters', None)

    @property
    def state(self):
        return self._data.get('state', None)

    @property
    def url(self):
        return self._data.get('url', None)

    def get_action(self, type):
        return self._actions[type]

    def _sub_action(self, subname):
        if not self.url:
            return None
        name = self._data.get(subname, None)
        if name and name.endswith('_'+subname.upper()):
            return name
        name = self.type + '_'+subname.upper()
        try:
            self.get_action(name)
            return name
        except KeyError:
            return None

    @property
    def start(self):
        return self._sub_action('start')

    @property
    def start_action(self):
        start = self.start
        if start is None:
            return None
        return self._actions.get(start, None)

    @property
    def error(self):
        return self._sub_action('error')

    @property
    def error_action(self):
        error = self.error
        if error is None:
            return None
        return self._actions.get(error, None)

    @property
    def success(self):
        return self._sub_action('success')

    @property
    def success_action(self):
        success = self.success
        if success is None:
            return None
        return self._actions.get(success, None)

    @property
    def method(self):
        if self.url:
            return self._data.get('method', 'get').lower()
        return None

    @property
    def use_csrf(self):
        return self.method is not None and self.method != 'get'

    def _selector(self, value):
        if not self.url:
            return None
        if value:
            return value
        items = self.type.split('_')
        if len(items) == 1:
            return None
        items = items[1:]
        items = [i.lower().capitalize() for i in items]
        items[0] = items[0].lower()
        return ''.join(items)

    @property
    def data_selector(self):
        return self._data.get('data_selector', None)

    @property
    def loading_selector(self):
        return self._data.get('loading_selector', None)

    @property
    def group(self):
        items = self.type.split('_')[:-1]
        return '_'.join(items)

    @property
    def operation(self):
        result = self._data.get('operation', None)
        if result:
            return result
        if self.url:
            return None
        action = self.type.lower()
        if action.startswith('fetch_') or action.startswith('retrieve_'):
            items = action.split('_')
            if len(items) > 2:
                if items[-2] == 'list':
                    return 'fetch'
                if wordnet_lemmatizer.lemmatize(items[-2], 'n') != items[-2]:
                    return 'fetch'
            return 'fetch'
        if action.startswith('add_') or action.startswith('create_'):
            return 'create'
        if actions.startswith('save_') or action.startswith('_update'):
            return 'update'
        if actions.startswith('delete') or action.startwith('remove'):
            return 'delete'
        return None

    @property
    def parent(self):
        items = self.type.split('_')
        condition = None
        if items[-1] == 'START':
            condition = lambda action: action.start == self.type
        elif items[-1] == 'ERROR':
            condition = lambda action: action.error == self.type
        elif items[-1] == 'SUCCESS':
            condition = lambda action: action.success == self.type
        else:
            return None
        return self._actions.filter(condition).values()[0]

    @property
    def object_type(self):
        result = self._data.get('object_type', None)
        if result not in ['item', 'collection', 'data', None]:
            print("Warning: Wrong object type for action {}.".format(self.type))
        if result:
            return result
        parent = self.parent
        if parent:
            return parent.object_type
        return 'data'

    @property
    def is_start(self):
        return self.type.endswith('_START')

    @property
    def is_error(self):
        return self.type.endswith('_ERROR')

    @property
    def is_success(self):
        return self.type.endswith('_SUCCESS')

    @property
    def reducer_type(self):
        if self.is_start:
            return self.operation + 'Start'
        elif self.is_error:
            return self.operation + 'Error'
        elif self.is_success:
            return self.operation + 'Success'
        return None


class States(Mapping):
    def __init__(self, data, params):
        data = [] if data is None else data['state']
        self._states = OrderedDict()
        for item in data:
            reducer = State(item, self)
            if reducer in self:
                raise ValueError("repeated reducer types: {}".format(reducer.type))
            self._states[reducer.type] = reducer
        assert isinstance(params, Parameters)
        self._params = params

    def __getitem__(self, type_name):
        return self._states[type_name]

    def __len__(self):
        return len(self._states)

    def __iter__(self):
        return iter(self._states)

    def __contains__(self, value):
        if isinstance(value, basestring):
            return value in self._states
        return value.type in self._states


class State(object):
    def __init__(self, data, states):
        if isinstance(data, basestring):
            data = {'type': data}
        self._data = data
        assert isinstance(states, States)
        self._states = states

    @property
    def type(self):
        return self._data['type']

    @property
    def actions(self):
        return self._states._params.actions.filter(lambda action: action.state==self.type)

    @property
    def has_ajax(self):
        return len(self.actions.with_url) > 0

    @property
    def selector_state(self):
        items = self._data.get('selectors', {})
        return items.get('state', 'getSelectorsState')

    @property
    def selector_data(self):
        items = self._data.get('selectors', {})
        result = items.get('data', 'get'+self.type.capitalize())
        if result == 'no':
            return None
        return result

    @property
    def selector_loading(self):
        items = self._data.get('selectors', {})
        result = items.get('loading', self.selector_data+'Loading')
        if result == 'no':
            return None
        return result

    @property
    def selector_error(self):
        items = self._data.get('selectors', {})
        result = items.get('error', self.selector_data+'Error')
        if result == 'no':
            return None
        return result

    @property
    def selector_by_id(self):
        if not self.is_collection:
            return None
        name = wordnet_lemmatizer.lemmatize(self.type, 'n')
        items = self._data.get('selectors', {})
        result = items.get('by_id', 'get'+name+'ById')
        if result == 'no':
            return None
        return result

    @property
    def is_collection(self):
        result = self._data.get('is_collection', True)
        return result == True or result.lower() == 'yes'

    @property
    def has_create_actions(self):
        return len(self.actions.filter(lambda action: action.operation=='create')) > 0

    @property
    def has_update_actions(self):
        return len(self.actions.filter(lambda action: action.operation=='update')) > 0

    @property
    def has_delete_actions(self):
        return len(self.actions.filter(lambda action: action.operation=='delete')) > 0


class Parameters(object):
    def __init__(self, data):
        self.actions = Actions(data, self)
        self.states = States(data, self)
