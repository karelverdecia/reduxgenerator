#
from io import StringIO
from contextlib import contextmanager


class GenMixin(object):
    def _start_level(self):
        if not hasattr(self, '_level'):
            self._level = 0

    def write(self, text):
        self._start_level()
        if isinstance(text, str):
            text = text.decode('utf-8')
        self._buffer.write(u'    '*self._level)
        self._buffer.write(text)

    def writeln(self, text=u''):
        self.write(text+u'\n')

    @contextmanager
    def level_up(self):
        self._start_level()
        try:
            self._level += 1
            yield
        finally:
            self._level -= 1

    def __call__(self):
        self._buffer = StringIO()
        self.write_body()
        return self._buffer.getvalue()
