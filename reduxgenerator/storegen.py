# -*- coding: utf8 -*-
from io import StringIO
from .mixins import GenMixin
from . import parameters

STORE_SRC = """import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware     from 'redux-thunk'
import { combineReducers } from 'redux-immutable';
import { reducer as form } from 'redux-form'
import Reducers            from './reducers';

export const configureStore = () => {
    const _Reducers = combineReducers({
        models: Reducers,
        form
    })

    const store = createStore(
        _Reducers,
        compose(applyMiddleware(thunkMiddleware), window.devToolsExtension ? window.devToolsExtension() : f => f)
    );
    return store;
}

export default configureStore;
"""



class StoreGen(GenMixin):
    def __init__(self, actions):
        assert isinstance(actions, parameters.Actions)
        self._actions = actions

    def __call__(self):
        self._buffer = StringIO()
        self.write(STORE_SRC)
        return self._buffer.getvalue()

