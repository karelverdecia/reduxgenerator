# -*- coding: utf-8 -*-
from io import StringIO
from .mixins import GenMixin
from . import parameters


class IndexMixin(GenMixin):
    def __init__(self, states):
        assert isinstance(states, parameters.States)
        self._states = states

    @property
    def actions(self):
        return self._states._params.actions

    @property
    def states(self):
        return self._states


class IndexGen(IndexMixin):
    def write_imports(self):
        self.writeln("import Immutable from 'immutable';")
        width = max([len(state) for state in self.states])
        for state in self.states:
            self.writeln("import {0}, * as from{1} from './{2}';".format(state.ljust(width),
                state.capitalize().ljust(width), state))
        self.writeln()
        self.writeln()

    def write_state(self):
        self.writeln("const INITIAL_STATE = Immutable.fromJS({")
        with self.level_up():
            for state in self.states.values():
                self.writeln(state.type+': {')
                with self.level_up():
                    self.writeln("data: null,")
                    self.writeln("error: null,")
                    self.writeln("loading: false,")
                    if state.has_create_actions:
                        self.writeln('create: {')
                        with self.level_up():
                            self.writeln("loading: false,")
                            self.writeln("error: null,")
                            self.writeln("id: null")
                        self.writeln('},')
                    if state.has_update_actions:
                        self.writeln('save: {')
                        with self.level_up():
                            self.writeln("loading: false,")
                            self.writeln("error: null,")
                            self.writeln("id: null")
                        self.writeln('},')
                    if state.has_delete_actions:
                        self.writeln('remove: {')
                        with self.level_up():
                            self.writeln("loading: false,")
                            self.writeln("error: null,")
                            self.writeln("id: null")
                        self.writeln('},')
                self.writeln("},")
        self.writeln("});")
        self.writeln()
        self.writeln()

    def write_reducer(self):
        self.writeln("function _reducer(state=INITIAL_STATE, action) {")
        with self.level_up():
            for state in self.states.values():
                self.writeln("state = {}(state, action);".format(state.type))
            self.writeln("return state;")
        self.writeln("}")
        self.writeln()
        self.writeln("export default _reducer;")
        self.writeln()

    def write_selectors(self):
        self.writeln("const getState = (state) => state.get('models');")
        self.writeln()
        for state in self.states.values():
            self.writeln("// {} selectors".format(state.type))
            if state.selector_data:
                self.writeln("export const {} = (state) =>".format(state.selector_data))
                self.writeln("    from{}.{}(getState(state));".format(state.type.capitalize(), state.selector_data))
                self.writeln()
            if state.selector_by_id:
                self.writeln("export const {} = (state, id) =>".format(state.selector_by_id))
                self.writeln("    from{}.{}(getState(state), id);".format(state.type.capitalize(), state.selector_by_id))
                self.writeln()
            if state.selector_loading:
                self.writeln("export const {} = (state) =>".format(state.selector_loading))
                self.writeln("    from{}.{}(getState(state));".format(state.type.capitalize(), state.selector_loading))
                self.writeln()
            if state.selector_loading:
                self.writeln("export const {} = (state) =>".format(state.selector_error))
                self.writeln("    from{}.{}(getState(state));".format(state.type.capitalize(), state.selector_error))
                self.writeln()
            self.writeln()

    def write_body(self):
        self.write_imports()
        self.write_state()
        self.write_reducer()
        self.write_selectors()


class ModuleReducersGen(GenMixin):
    def __init__(self, state):
        assert isinstance(state, parameters.State)
        self._state = state

    @property
    def state(self):
        return self._state

    @property
    def actions(self):
        return self._state.actions

    def write_imports(self):
        self.writeln("import * as fromUtils from './utils';")
        self.writeln("import {")
        with self.level_up():
            for action in self.actions.without_url:
                self.writeln(action+',')
        self.writeln("} from '../action-types';")
        for action in self.actions:
            pass
        self.writeln()
        self.writeln()

    def write_reducers(self):
        self.writeln('// reducers')
        for name in self.actions.without_url:
            action = self.actions[name]
            print action.reducer_type
            if action.reducer_type:
                self.writeln("const {} = fromUtils.{}('{}', {});".format(action.action_name,
                    action.reducer_type, self._state.type, name))
        self.writeln()
        self.writeln()

    def write_reducer_index(self):
        self.writeln("const reducer = (state, action) => {")
        with self.level_up():
            self.writeln(" switch (action.type) {")
            with self.level_up():
                for name in self.actions.without_url:
                    action = self.actions[name]
                    self.writeln("case {}:".format(name))
                    self.writeln("    return {}(state, action);".format(action.action_name))
                self.writeln("default:")
                self.writeln("    return state;")
            self.writeln("}")
        self.writeln("}")
        self.writeln()
        self.write("export default reducer;")
        self.writeln()
        self.writeln()

    def write_selectors(self):
        self.writeln("// selectors")
        state = self._state
        if state.selector_data:
            self.writeln("export const {} = fromUtils.getEntities('{}');".format(state.selector_data, state.type))
            self.writeln()
        if state.selector_by_id:
            self.writeln("export const {} = fromUtils.getEntityById('{}');".format(state.selector_by_id, state.type))
            self.writeln()
        if state.selector_loading:
            self.writeln("export const {} = fromUtils.getEntitiesLoading('{}');".format(state.selector_loading, state.type))
            self.writeln()
        if state.selector_loading:
            self.writeln("export const {} = fromUtils.getEntitiesError('{}');".format(state.selector_error, state.type))
            self.writeln()
        self.writeln()

    def write_body(self):
        self.write_imports()
        self.write_reducers()
        self.write_reducer_index()
        self.write_selectors()


class ReducersGen(GenMixin):
    def __init__(self, params):
        assert isinstance(params, parameters.Parameters)
        self._params = params

    def __call__(self):
        index_gen = IndexGen(self._params.states)
        yield ('reducers/index.js', index_gen())
        for state in self._params.states.values():
            if state.type not in self._params.actions.states:
                print("Warning: State '{}' not used in any action.".format(state.type))
            generator = ModuleReducersGen(state)
            yield ('reducers/{}.js'.format(state.type), generator())

