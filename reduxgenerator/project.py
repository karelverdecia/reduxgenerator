# -*- coding: utf-8 -*-
import os
import yaml
import hashlib
import tempfile
from . import parameters


class Project(object):
    def __init__(self, path):
        if not os.path.isfile(path):
            raise ValueError("Project path must be a file.")
        self.path = path
        with open(path) as stream:
            src = stream.read()
        data = yaml.load(src)
        self.params = parameters.Parameters(data)

    @property
    def base_dir(self):
        return os.path.dirname(self.path)

    def create_dirs(self, base_dir=None):
        if not base_dir:
            base_dir = self.base_dir
        if not os.path.exists(base_dir):
            os.mkdir(base_dir)
        actions_dir = os.path.join(base_dir, 'actions')
        if not os.path.exists(actions_dir):
            os.mkdir(actions_dir)
        reducers_dir = os.path.join(base_dir, 'reducers')
        if not os.path.exists(reducers_dir):
            os.mkdir(reducers_dir)

    @property
    def tmp_dir(self):
        h = hashlib.md5(self.path)
        return os.path.join(tempfile.gettempdir(), h.hexdigest())

    def create_tmp_dirs(self):
        self.create_dirs(self.tmp_dir)

    def write_file(self, name, content):
        path = os.path.join(self.base_dir, name)
        if getattr(self, '_overwrite', False) or not os.path.exists(path):
            with open(path, 'w') as stream:
                stream.write(content)
        tmp_path = os.path.join(self.tmp_dir, name)
        with open(tmp_path, 'w') as stream:
            stream.write(content)

    def gen_action_types(self):
        from .actiontypesgen import ActionTypes
        action_types = ActionTypes(self.params.actions)
        self.write_file('action-types.js', action_types())

    def _gen_files(self, generator_cls):
        generator = generator_cls(self.params)
        for name, content in generator():
            self.write_file(name, content)

    def gen_actions(self):
        from .actionsgen import ActionsGen
        self._gen_files(ActionsGen)

    def gen_reducers(self):
        from .reducersgen import ReducersGen
        self._gen_files(ReducersGen)

    def gen_store(self):
        from .storegen import StoreGen
        store_gen = StoreGen(self.params.actions)
        self.write_file('store.js', store_gen())

    def __call__(self):
        self.create_dirs()
        self.create_tmp_dirs()
        self.gen_action_types()
        self.gen_actions()
        self.gen_reducers()
        self.gen_store()
